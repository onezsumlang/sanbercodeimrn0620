{/*Soal 1*/}
console.log('Soal 1====================');

class Animal {
	constructor(name){
		this.aname = name;
		this.alegs = 4;
		this.acold_blooded = false;
	}
	
	get name(){
		return this.aname;
	}
	
	get legs(){
		return this.alegs;
	}
	
	get cold_blooded(){
		return this.acold_blooded;
	}
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('\n');

class Frog extends Animal {
	constructor(name){
		super(name);
	}
	
	jump(){
		console.log("hop hop");
	}
}

class Ape extends Animal {
	constructor(name){
		super(name);
	}
	
	yell(){
		console.log("Auooo");
	}
}

var sungokong = new Ape("kera sakti");
// console.log(sungokong.name);
sungokong.yell(); 
 
var kodok = new Frog("buduk")
kodok.jump(); 

console.log('\n');

{/*Soal 2*/}
console.log('Soal 2====================');

class Clock {	
	constructor({ template }) {
		this.template = template;
		this.timer;
	}

	render() {		
		var date = new Date();

		var hours = date.getHours();
		if (hours < 10) hours = '0' + hours;

		var mins = date.getMinutes();
		if (mins < 10) mins = '0' + mins;

		var secs = date.getSeconds();
		if (secs < 10) secs = '0' + secs;

		var output = this.template
		.replace('h', hours)
		.replace('m', mins)
		.replace('s', secs);

		console.log(output);
	}

	stop() {
		clearInterval(this.timer);
	}

	start() {
		this.render();
		this.timer = setInterval(() => this.render(), 1000);
	}
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 