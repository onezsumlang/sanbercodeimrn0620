var readBooks = require('./callback.js');

var books = [
	{name: 'LOTR', timeSpent: 3000},
	{name: 'Fidas', timeSpent: 2000},
	{name: 'Kalkulus', timeSpent: 4000}
];

var startTime = 10000;

console.log('Reading Time ' + startTime + '\n');

function reading(i){
	i = (i ? i : 0);
	if(!books || i == books.length){ 
		console.log('No Books in queue'); 
		return 0;
	}
	
	return readBooks(startTime, books[i], function(e){
		startTime = e;
		i = i+1;
		if(i<books.length){
			reading(i);
		}
	});
}

reading();