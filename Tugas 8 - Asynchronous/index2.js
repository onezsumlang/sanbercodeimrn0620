var readBooksPromise = require('./promise.js');

var books = [
	{name: 'LOTR', timeSpent: 3000},
	{name: 'Fidas', timeSpent: 2000},
	{name: 'Kalkulus', timeSpent: 4000}
];


var startTime = 10000;
var flag = 0;

console.log('Reading Time ' + startTime + '\n');

function reading(i){	
	i = (i ? i : 0);
	
	if(i == books.length){ 
		console.log('No Books in queue'); 
		return 0;
	}
	
	setTimeout(function(){
		readBooksPromise(startTime, books[i])
			.then(function(fulfilled){
				return fulfilled;
			})
			.catch(function(error){
				return error;
			});
			
	
		startTime -= books[i].timeSpent;
		flag = books[i].timeSpent;
		i++;
		if(i<books.length){
			reading(i);
		}
	}, flag);
}

reading();