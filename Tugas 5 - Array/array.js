{/*Soal 1*/}
console.log('Soal 1====================');

function range(startNum, finishNum){
	var arr = [];

	switch(true){
		case(startNum < finishNum) : {
			do{
				arr.push(startNum);
				startNum++;
			}while(startNum <= finishNum);
			break;
		}
		case(startNum > finishNum) : {
			do{
				arr.push(startNum);
				startNum--;
			}while(startNum >= finishNum);
			break;
		}
		default: { arr = -1; }
	}
	return arr;
}

console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range());

console.log('\n');

{/*Soal 2*/}
console.log('Soal 2====================');

function rangeWithStep(startNum, finishNum, step){
	var arr = [];

	var step = (step ? step : 1);
	
	var temp = startNum;

	switch(true){
		case(startNum < finishNum) : {
			do{
				arr.push(startNum);
				startNum += step;
			}while(startNum <= finishNum);
			break;
		}
		case(startNum > finishNum) : {
			do{
				arr.push(startNum);
				startNum -= step;
			}while(startNum >= finishNum);
			break;
		}
		default: { arr = -1; }
	}
	return arr;
}

console.log(rangeWithStep(1,10,2));
console.log(rangeWithStep(11,23,3));
console.log(rangeWithStep(5,2,1));
console.log(rangeWithStep(29,2,4));

console.log('\n');

{/*Soal 3*/}
console.log('Soal 3====================');

function sum(startNum, finishNum, step){
	var arr = [];
	var temp = 0;
	var numLength = 0;

	if(finishNum){
		arr = rangeWithStep(startNum, finishNum, step);
		numLength = arr.length;
		for(i=0;i<numLength;i++){
			temp += arr[i];
		}
	}else if(startNum){
		temp = startNum;
	}else{
		temp = 0;
	}
	return temp;
}

console.log(sum(1,10));
console.log(sum(5,50,2));
console.log(sum(15,10));
console.log(sum(20,10,2));
console.log(sum(1));
console.log(sum());
console.log(sum(5));

console.log('\n');

{/*Soal 4*/}
console.log('Soal 4====================');

function dataHandling(input){
	var str = '';
	for(i=0;i<input.length;i++){
		str += 'Nomor ID: ' + input[i][0] + '\n';
		str += 'Nama Lengkap: ' + input[i][1] + '\n';
		str += 'TTL: ' + input[i][2] + ' ' + input[i][3] + '\n';
		str += 'Hobi: ' + input[i][4] + '\n';
		str += '\n\n';
	}	
	return str;
}

var input = [
	["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
	["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
	["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
	["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

console.log(dataHandling(input));

{/*Soal 5*/}
console.log('Soal 5====================');

function balikKata(str){
	var newStr = '';
	for(i=str.length-1;i>=0;i--){
		newStr += str[i];
	}
	return newStr;
}

console.log(balikKata('Kasur Rusak'));
console.log(balikKata('SanberCode'));
console.log(balikKata('Haji Ijah'));
console.log(balikKata('racecar'));
console.log(balikKata('I am Sanbers'));

console.log('\n');

{/*Soal 6*/}
console.log('Soal 6====================');

function getMonth(input){
	var month;
	switch(input){
		case '01': { month = 'Januari'; break; }
		case '02': { month = 'Februari'; break; }
		case '03': { month = 'Maret'; break; }
		case '04': { month = 'April'; break; }
		case '05': { month = 'Mei'; break; }
		case '06': { month = 'Juni'; break; }
		case '07': { month = 'Juli'; break; }
		case '08': { month = 'Agustus'; break; }
		case '09': { month = 'September'; break; }
		case '10': { month = 'Oktober'; break; }
		case '11': { month = 'November'; break; }
		case '12': { month = 'Desember'; break; }
		default: { month = 'wrong input'; }
	}
	return month;
}

function dataHandling2(input){
	input.splice(1, 1, input[1] + ' Elsharawy');
	input.splice(2, 1, 'Provinsi ' + input[2]);
	input.splice(4, 1, 'Pria', 'SMA Internasional Metro');
	console.log(input);
	
	var birth = input[3].split('/');
	console.log(getMonth(birth[1]));

	var birth2 = birth.sort( function (value1, value2) { return value2 - value1 } );
	console.log(birth2);

	var birth3 = input[3].split('/').join('-');
	console.log(birth3);

	var name = input[1].slice(0,14);
	console.log(name);
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
