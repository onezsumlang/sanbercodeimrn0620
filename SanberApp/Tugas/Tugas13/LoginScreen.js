import React, {Component } from 'react';
import { 
    StyleSheet, Text, View, Image,
    TouchableOpacity, ImageBackground,
    TextInput, CheckBox, Button
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
const loginBacground = './images/login_background.png';

export default class LoginScreen extends Component {
    render(){
        return (
            <View style={styles.container}>
                <ImageBackground source={require(loginBacground)} style={{width: '100%', height: '100%'}}>
                    <View style={styles.logoContainer}>
                        <Image source={require('./images/logo.png')}/>
                    </View>
                    <View style={styles.boxLoginContainer}>
                        <View style={styles.boxLogin}>
                            <Text style={styles.loginHeader}>Authentication Required</Text>
                            
                            <TextInput 
                                style={styles.inputText}
                                placeholder="Username or Email"                                
                            />
                            <TextInput 
                                style={styles.inputText}
                                placeholder="Password"                                
                            />
                            <TouchableOpacity style={styles.btnWrapper}>
                                <Button title="SIGN IN" color="#11999E" style={styles.btnLogin} />
                            </TouchableOpacity>

                            <View style={{flexDirection: 'row'}}>
                                <Text>Don't have an account?</Text>
                                <Text style={{color: '#11999E', fontWeight: 'bold'}}> Sign Up</Text>
                            </View>
                            
                            <Text style={{marginVertical:10}}>- or -</Text>

                            <TouchableOpacity style={styles.btnWrapper}>
                                <Button title="SIGN IN With Facebook" color="#3B5998" style={styles.btnLogin} />
                            </TouchableOpacity>

                            <TouchableOpacity style={styles.btnWrapper}>
                                <Button title="SIGN IN With Google" color="red" />
                            </TouchableOpacity>
                        </View>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop:25 
    },
    logoContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        paddingTop: 50
    },
    boxLoginContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    boxLogin: {
        width:370, paddingVertical: 30,
        backgroundColor:'#fff', alignItems: 'center'
    },
    loginHeader: {
        fontSize: 22,
        marginBottom: 30
    },
    inputText: {
        width:320, height:40,
        marginBottom: 20, paddingHorizontal: 10,
        borderColor: '#ccc', borderWidth: 1
    },
    btnWrapper: {
        width: 320,
        marginBottom: 5,
        backgroundColor: 'red'
    },
});