import React, { Component } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, Button
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class AboutScreen extends Component {
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.aboutHeader}>
                    <Image source={require('./images/profile_pic.png')} style={styles.headerImage}/>
                    <View style={styles.headerName}>
                        <Text style={{fontSize:24, color: '#fff'}}>Michael Onesimus Sumlang</Text>
                        <Text style={{fontSize:14, color: '#fff', fontStyle: 'italic'}}>Programmer</Text>
                    </View>                    
                </View>
                <View style={styles.wrapperProjectYears}>
                    <View style={styles.projectYearsContent}>
                        <Text style={styles.projectYearCount}>66</Text>
                        <Text style={styles.projectYearTitle}>projects</Text>
                    </View>
                    <View style={styles.projectYearsContent}>
                        <Text style={styles.projectYearCount}>10</Text>
                        <Text style={styles.projectYearTitle}>years experience</Text>
                    </View>
                </View>
                <View style={styles.btnContainer}>
                    <TouchableOpacity style={styles.btnWrapper}>
                        <Button title="VIEW SKILL" color="#11999E"/>
                    </TouchableOpacity>
                </View>
                <View style={styles.bodyContainer}>
                    <Text style={styles.contentTitle}>Basic information</Text>
                    <View style={styles.infoWrapper}>
                        <Icon name="map-marker" size={25} color="#11999E"/>
                        <Text style={styles.contentInfo}>Jakarta, Indonesia</Text>
                    </View>
                    <View style={styles.infoWrapper}>
                        <Icon name="email" size={25} color="#11999E"/>
                        <Text style={styles.contentInfo}>onez.sumlang@gmail.com</Text>
                    </View>
                    <View style={styles.infoWrapper}>
                        <Icon name="phone" size={25} color="#11999E"/>
                        <Text style={styles.contentInfo}>+62 812 859 888 47</Text>
                    </View>
                </View>

                <View style={{flexDirection:'row'}}>
                    <View style={{width: '50%'}}>
                        <View style={styles.bodyContainer}>
                            <Text style={styles.contentTitle}>Social Media</Text>
                            <View style={styles.infoWrapper}>
                                <Icon name="instagram" size={25} color="#11999E"/>
                                <Text style={styles.contentInfo}>/onezsumlang</Text>
                            </View>
                            <View style={styles.infoWrapper}>
                                <Icon name="facebook" size={25} color="#11999E"/>
                                <Text style={styles.contentInfo}>/onezsumlang</Text>
                            </View>
                            <View style={styles.infoWrapper}>
                                <Icon name="twitter" size={25} color="#11999E"/>
                                <Text style={styles.contentInfo}>/onezsumlang</Text>
                            </View>
                            <View style={styles.infoWrapper}>
                                <Icon name="youtube" size={25} color="#11999E"/>
                                <Text style={styles.contentInfo}>/user/onessumlang</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{width: '50%'}}>
                        <View style={styles.bodyContainer}>
                            <Text style={styles.contentTitle}>Git Project</Text>
                            <View style={styles.infoWrapper}>
                                <Icon name="github-circle" size={25} color="#11999E"/>
                                <Text style={styles.contentInfo}>/onezsumlang</Text>
                            </View>
                            <View style={styles.infoWrapper}>
                                <Icon name="gitlab" size={25} color="#11999E"/>
                                <Text style={styles.contentInfo}>/onezsumlang</Text>
                            </View>
                        </View>
                    </View>
                </View>
                
                <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="home" size={25} color="#11999E"/>
                        <Text style={styles.tabTitle}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="account" size={25} color="#11999E"/>
                        <Text style={styles.tabTitle}>Profile</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="power" size={25} color="#11999E"/>
                        <Text style={styles.tabTitle}>Sign Out</Text>
                    </TouchableOpacity>
                </View>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop:25 
    },
    aboutHeader: {
        height: 200,
        backgroundColor: '#40514E',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerImage: {
        width:100, height:100
    },
    headerName: {
        flexDirection: 'column', alignItems: 'center'
    },
    wrapperProjectYears: {
        height:74, backgroundColor: '#E4F9F5', flexDirection: 'row'
    },
    projectYearsContent: {
        width:'50%', borderWidth: 0.5, borderColor: '#ccc', alignItems: 'center', justifyContent: 'center'
    },
    projectYearCount: {
        fontSize:30, color: '#aaa'
    },
    projectYearTitle: {
        fontSize:18, fontStyle: 'italic', color: '#aaa'
    },
    btnContainer: {
        alignItems:'center', marginVertical:15
    },
    btnWrapper: {
        width: 320,
        marginBottom: 5,
        backgroundColor: 'red'
    },
    bodyContainer: {
        paddingHorizontal: 20
    },
    contentTitle: {
        fontSize:16, marginBottom: 5
    },
    infoWrapper: {
        paddingLeft: 20, 
        flexDirection: 'row', alignItems: 'center',
        marginBottom:5
    },
    contentInfo: {
        fontSize:16, paddingLeft: 5
    },
    tabBar: {
        height: 40,
        backgroundColor: '#fff',
        borderTopWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabTitle: {
        fontSize: 11,
        color: '#11999E',
        paddingTop: 3
    }
}); 