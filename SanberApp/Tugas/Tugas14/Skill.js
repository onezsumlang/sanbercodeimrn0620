import React, { Component } from 'react';
import {
    StyleSheet, View, Text, TouchableOpacity,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Skill extends Component {
    render(){
        return (
                <View style={styles.skillContainer}>
                    <View style={styles.skillCategory}>
                        <Text style={styles.categoryTitle}>Programming Language</Text>
                        <View style={styles.iconDropdown}>
                            <TouchableOpacity>
                                <Icon name="chevron-down" size={25} color="#fff"/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.skillListContainer}>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Text style={styles.skillTitle}>PHP Native</Text>
                                <Icon name="language-php" size={25} color="#11999E" style={{marginLeft: 15}}/>
                            </View>
                            <Text style={styles.skillLevel}>Advance</Text>
                        </View>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                            </View>
                        </View>
                    </View>
                    <View style={styles.skillListContainer}>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Text style={styles.skillTitle}>HTML, CSS</Text>
                                <Icon name="language-html5" size={25} color="#11999E" style={{marginLeft: 15}}/>
                                <Icon name="language-css3" size={25} color="#11999E" style={{marginLeft: 15}}/>
                            </View>
                            <Text style={styles.skillLevel}>Advance</Text>
                        </View>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                            </View>
                        </View>
                    </View>
                    <View style={styles.skillListContainer}>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Text style={styles.skillTitle}>JavaScript</Text>
                                <Icon name="language-javascript" size={25} color="#11999E" style={{marginLeft: 15}}/>
                            </View>
                            <Text style={styles.skillLevel}>Intermediate</Text>
                        </View>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                            </View>
                        </View>
                    </View>
                    <View style={styles.skillListContainer}>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Text style={styles.skillTitle}>SQL</Text>
                                <Icon name="database" size={25} color="#11999E" style={{marginLeft: 15}}/>
                            </View>
                            <Text style={styles.skillLevel}>Intermediate</Text>
                        </View>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                            </View>
                        </View>
                    </View>

                    <View style={styles.skillCategory}>
                        <Text style={styles.categoryTitle}>Framework / Library</Text>
                        <View style={styles.iconDropdown}>
                            <TouchableOpacity>
                                <Icon name="chevron-down" size={25} color="#fff"/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.skillListContainer}>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Text style={styles.skillTitle}>CodeIgniter</Text>
                            </View>
                            <Text style={styles.skillLevel}>Intermediate</Text>
                        </View>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                            </View>
                        </View>
                    </View>
                    <View style={styles.skillListContainer}>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Text style={styles.skillTitle}>Bootstrap</Text>
                                <Icon name="bootstrap" size={25} color="#11999E" style={{marginLeft: 15}}/>
                            </View>
                            <Text style={styles.skillLevel}>Intermediate</Text>
                        </View>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                            </View>
                        </View>
                    </View>
                    <View style={styles.skillListContainer}>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Text style={styles.skillTitle}>jQuery/Ajax</Text>
                                <Icon name="jquery" size={25} color="#11999E" style={{marginLeft: 15}}/>
                            </View>
                            <Text style={styles.skillLevel}>Intermediate</Text>
                        </View>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                            </View>
                        </View>
                    </View>
                    <View style={styles.skillListContainer}>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Text style={styles.skillTitle}>Ionic</Text>
                            </View>
                            <Text style={styles.skillLevel}>Basic</Text>
                        </View>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                            </View>
                        </View>
                    </View>
                    
                    <View style={styles.skillListContainer}>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Text style={styles.skillTitle}>React Native</Text>
                                <Icon name="react" size={25} color="#11999E" style={{marginLeft: 15}}/>
                            </View>
                            <Text style={styles.skillLevel}>Basic</Text>
                        </View>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                            </View>
                        </View>
                    </View>

                    <View style={styles.skillCategory}>
                        <Text style={styles.categoryTitle}>Technology</Text>
                        <View style={styles.iconDropdown}>
                            <TouchableOpacity>
                                <Icon name="chevron-down" size={25} color="#fff"/>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.skillListContainer}>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Text style={styles.skillTitle}>Git</Text>
                            </View>
                            <Text style={styles.skillLevel}>Basic</Text>
                        </View>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                            </View>
                        </View>
                    </View>
                    <View style={styles.skillListContainer}>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Text style={styles.skillTitle}>Github</Text>
                            </View>
                            <Text style={styles.skillLevel}>Basic</Text>
                        </View>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                            </View>
                        </View>
                    </View>
                    <View style={styles.skillListContainer}>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Text style={styles.skillTitle}>Gitlab</Text>
                                <Icon name="gitlab" size={25} color="#11999E" style={{marginLeft: 15}}/>
                            </View>
                            <Text style={styles.skillLevel}>Basic</Text>
                        </View>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                            </View>
                        </View>
                    </View>
                    <View style={styles.skillListContainer}>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Text style={styles.skillTitle}>Android Studio</Text>
                                <Icon name="android" size={25} color="#11999E" style={{marginLeft: 15}}/>
                            </View>
                            <Text style={styles.skillLevel}>Basic</Text>
                        </View>
                        <View style={styles.skillWrap}>
                            <View style={{                            
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}>
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                                <Icon name="star-outline" size={25} color="#11999E" />
                            </View>
                        </View>
                    </View>
                </View>
        );
    }
}

const styles = StyleSheet.create({
    skillContainer: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        alignItems: 'center'
    },
    skillCategory: {
        width: '100%',
        backgroundColor: '#30E3CA',
        padding: 10,
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    categoryTitle: {
        color: '#fff',
        fontSize: 18
    },
    skillListContainer: {
        width: '100%',
        paddingHorizontal: 20,
        paddingVertical: 10,
        backgroundColor: '#E4F9F5',
    },
    skillWrap: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    skillTitle: {
        fontSize: 15,
    },
    skillLevel: {
        fontSize: 15,
        fontStyle: 'italic',
        color: '#888'
    }
});