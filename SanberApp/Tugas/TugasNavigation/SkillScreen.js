import React, { Component } from 'react';
import {
    StyleSheet, View, Text, Image, TouchableOpacity, ScrollView
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Skill from './Skill';

export default class AboutScreen extends Component {
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={{flexDirection: 'row', alignItems: 'center', paddingHorizontal: 20}}>
                        <Icon name="arrow-left" size={25} color="#fff"/>
                        <Text style={styles.headerText}>View Skill</Text>
                    </View>
                </View>

                <View style={styles.body}>
                    <ScrollView>
                        <Skill tes="asdf"/>
                    </ScrollView>
                    
                </View>
                

                <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="home" size={25} color="#11999E"/>
                        <Text style={styles.tabTitle}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="account" size={25} color="#11999E"/>
                        <Text style={styles.tabTitle}>Profile</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="power" size={25} color="#11999E"/>
                        <Text style={styles.tabTitle}>Sign Out</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop:25 
    },
    header: {
      backgroundColor: '#40514E',
      justifyContent: 'center',
    },
    headerText: {
      color: 'white',
      fontSize: 18,
      padding: 15
    },
    body: {
        paddingBottom: 100
    },
    tabBar: {
        position: 'absolute',
        bottom: 0, left: 0, right: 0,
        height: 40,
        backgroundColor: '#fff',
        borderTopWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabTitle: {
        fontSize: 11,
        color: '#11999E',
        paddingTop: 3
    }
}); 