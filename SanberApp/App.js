import React, {Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import YoutubeUI from './Tugas/Tugas12/App';
import LoginScreen from './Tugas/Tugas13/LoginScreen';
import RegisterScreen from './Tugas/Tugas13/RegisterScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';

import TodoNotes from './Tugas/Tugas14/App';
import SkillScreen from './Tugas/Tugas14/SkillScreen';

import Latihan15 from './Latihan/Tugas15/index';
import Tugas15 from './Tugas/Tugas15/index';
import TugasNavigation from './Tugas/TugasNavigation';

import Quiz3 from './Tugas/Quiz3';

export default class App extends Component {
  render() {
    return ( 
    //   <YoutubeUI/>
        // <LoginScreen/>
        // <RegisterScreen/>
        // <AboutScreen/>

        // <TodoNotes/>
        // <SkillScreen/>

        // <Latihan15 />
        // <Tugas15 />
        // <TugasNavigation />  

        <Quiz3 />
    );
  }
}

const styles = StyleSheet.create({
});