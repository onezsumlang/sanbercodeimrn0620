{/*Soal 1*/}
console.log('Soal 1====================');

function arrayToObject(arr){
	if(arr.length > 0){ 
		var obj = [];
		var str = '';
		
		var now = new Date()
		var thisYear = now.getFullYear()
		
		for(i=0;i<arr.length;i++){
			let objName = arr[i][0] + ' ' + arr[i][1];
			str = (i+1) + '. ' + objName + ' : ';
			
			let birthYear = arr[i][3];
			
			age = ((birthYear > thisYear) || !birthYear ? 'Invalid Birth Year' : (thisYear-birthYear));
			
			obj = {
				firstName : arr[i][0],
				lastName : arr[i][1],
				gender : arr[i][2],
				age : age
			}
			console.log(str, obj);
		}
	}
}

var arr = [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]];
arrayToObject(arr);

console.log('\n');

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]];
arrayToObject(people);

console.log('\n');

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]];
arrayToObject(people2);

console.log('\n');

// Error case 
arrayToObject([]);

console.log('\n');

{/*Soal 2*/}
console.log('Soal 2====================');

function shoppingTime(memberId, money){
	if(!memberId){
		return 'Mohon maaf, toko X hanya berlaku untuk member saja';
	}
	
	if(money && money < 50000){
		return 'Mohon maaf, uang tidak cukup';
	}
	
	var sale = [
		{ name: 'Sepatu Stacattu', price : 1500000 },
		{ name: 'Baju Zoro', price : 500000 },
		{ name: 'Baju H&N', price : 250000 },
		{ name: 'Sweater Uniklooh', price : 175000 },
		{ name: 'Casing Handphone', price : 50000 }
	];
	
	var listPurchased = [];
	var changeMoney = money;
	
	for(i=0; i<sale.length; i++){
		if(money - sale[i].price >= 0){
			listPurchased.push(sale[i].name);
			changeMoney -= sale[i].price;
		}
	}
	
	var obj = {
		memberId : memberId,
		money: money,
		listPurchased : listPurchased,
		changeMoney : changeMoney
	};
	return obj;
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));

console.log('\n');

console.log(shoppingTime('82Ku8Ma742', 170000));

console.log('\n');

console.log(shoppingTime('', 2475000)); 

console.log('\n');

console.log(shoppingTime('234JdhweRxa53', 15000)); 

console.log('\n');

console.log(shoppingTime()); 

console.log('\n');

{/*Soal 3*/}
console.log('Soal 3====================');

function naikAngkot(listPenumpang){
	var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
	
	var totalPenumpang = listPenumpang.length;
	
	var output = [];
	
	for(i=0; i<listPenumpang.length; i++){
		let ongkos = 2000;
		let naikDari = rute.indexOf(listPenumpang[i][1]);
		let tujuan = rute.indexOf(listPenumpang[i][2]);
		
		let rutePassed = tujuan - naikDari;
		ongkos = ongkos * rutePassed;
		
		output.push({
			penumpang : listPenumpang[i][0],
			naikDari : listPenumpang[i][1],
			tujuan : listPenumpang[i][2],
			bayar : ongkos
		});
	}
	
	return output;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));

console.log('\n');

console.log(naikAngkot([])); 