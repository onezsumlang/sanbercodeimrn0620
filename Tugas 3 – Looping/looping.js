{/*Soal 1*/}
console.log('Soal 1====================');
console.log('LOOPING PERTAMA');

var x = 1;
while(x <= 20){
	if(x%2 == 0){
		console.log(x + ' - I love coding');
	}
	x++;
}

console.log('\n');

console.log('LOOPING KEDUA');

while(x > 0){
	if(x%2 == 0){
		console.log(x + ' - I will become a mobile developer');
	}
	x--;
}

console.log('\n');

{/*Soal 2*/}
console.log('Soal 2====================');
console.log('OUTPUT');

for(y=1;y<=20;y++){
	if(y%2 == 0){
		console.log(y + '- Berkualitas');
	}else if(y%2 !== 0 && y%3 == 0){
		console.log(y + '- I Love Coding');
	}else{
		console.log(y + '- Santai');
	}
}

console.log('\n');

{/*Soal 3*/}
console.log('Soal 3====================');
console.log('OUTPUT');

var str = '';
var i=1;
do{
	for(j=1;j<=8;j++){
		str += '#';
	}
	console.log(str);
	str = '';
	i++;
}while(i<=4);

console.log('\n');

{/*Soal 4*/}
console.log('Soal 4====================');
console.log('OUTPUT');

var str = '';
for(i=1;i<=7;i++){
	for(j=i;j<=i;j++){
		str += '#';
		console.log(str);
	}
}

console.log('\n');

{/*Soal 5*/}
console.log('Soal 5====================');
console.log('OUTPUT');

var str = '';
var i=1;
var flag=1;
while(i<=8){
	for(j=1;j<=8;j++){
		flag++;
		if(flag%2 == 0){
			str += ' ';
		}else{
			str += '#';
		}
	}
	console.log(str);
	str = '';
	i++;
	flag++;
} 
